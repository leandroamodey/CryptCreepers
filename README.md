# Crypt Creepers (Pending Name)

This is a learning and practice game I started to make for an Online Course about Unity 2D. 

The main goal is to improve the base game and that begginers in Game Development (or not so much) can contribute freely to share in their portfolios.

## Installation

Just clone the repository with a Git client and open the project folder with Unity :)

The current version of the project is **2021.3.11f1**

## The Game

This is a very serious Hack'n'Slash game with a little skeleton who have to survive a night in the cementery to recover his life.

To do that he just count with a gun and all kind of stranges powers!

This is just the beggining of a really long and complex story . . .

...

Just kidding xD I just invented that to give context to the game, feel free to contribute to the narrative too, or just create some crazy mechanic without any sense, as long as you keep learning, thats your choice!

## Contributing
To contribute to the project and add some new mechanic, art, music, story or any other correction to the repository you can get in contact with me by Discord to show me what you did and then I will add you to the Project Members :)

If you just want to report any bug, or just make a suggestion you can make it through the Issues section of this repository.

Lastly, this is my first time trying to make a "public and Open Source project" so, any comment or suggestion of how I should manage all this is very welcomed.

That's all! Have fun and I hope to help and be helped by a lot of people!

## License
[MIT](https://choosealicense.com/licenses/mit/)
